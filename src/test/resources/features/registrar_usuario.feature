#Author: andresfernandolopezavila@gmail.com
@Regresion
Feature: Casos de prueba para registrar un usuario para accenture

  @RegistroUsuario
  Scenario Outline: Registrar un usuario con informacion completa
    Given Yo deseo registrar un usuario
      | id   | nombre   | segundo_nombre   | apellido   | segundo_apellido   | id_colegio   |
      | <id> | <nombre> | <segundo_nombre> | <apellido> | <segundo_apellido> | <id_colegio> |
    When Yo consumo el servicio POST de creacion de usuario
    Then Yo verifico la respuesta del servicio <respuesta>
    And Yo verifico que haya sido creado el usuario <respuesta_verificacion>

    Examples: 
      | id | nombre | segundo_nombre | apellido | segundo_apellido | id_colegio | respuesta | respuesta_verificacion |
      |  8 | Pablo  | Fernando       | lopez    | Avila            |          3 |       201 |                    200 |
      |  9 | Juan   | Pablo          | Viancha  | Gallego          |          3 |       201 |                    200 |
