#Author: andresfernandolopezavila@gmail.com
@Regresion
Feature: Eliminacion de usuarios para accenture

  @RegistroUsuario
  Scenario Outline: Eliminar un usuario por medio del id
    Given Yo como cliente deseo eliminar el usuario <id>
    When Yo consumo el servicio DELETE
    Then Yo verifico la respuesta del servicio <respuesta>
    And Yo consulto el servicio GET para verificar la eliminacion con la <respuesta_verificacion>

    Examples: 
      | id | respuesta | respuesta_verificacion |
      |  8 |       200 |                    404 |
