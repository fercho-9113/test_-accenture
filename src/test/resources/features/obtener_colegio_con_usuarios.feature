#Author: andresfernandolopezavila@gmail.com
@Regresion
Feature: Obtener los dos primeros colegios y todos los usuarios de estos

  @DosColegiosConUsuarios
  Scenario Outline: Obtener colegios y usuarios
    Given Yo consumo el servicio para listar los colegios y tomar los <numero> primeros
    And Yo consumo el servicio de obtener usuarios por colegio
    Then Yo verifico la respuesta del servicio <respuesta>

    Examples: 
      | numero | respuesta |
      |      2 |       200 |