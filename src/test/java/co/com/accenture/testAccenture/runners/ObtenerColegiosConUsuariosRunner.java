package co.com.accenture.testAccenture.runners;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features ="src/test/resources/features/obtener_colegio_con_usuarios.feature",
glue="co.com.accenture.testAccenture.definitions")
public class ObtenerColegiosConUsuariosRunner {

}
