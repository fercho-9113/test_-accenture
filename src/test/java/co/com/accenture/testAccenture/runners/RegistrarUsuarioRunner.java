package co.com.accenture.testAccenture.runners;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features ="src/test/resources/features/registrar_usuario.feature",
glue="co.com.accenture.testAccenture.definitions")
public class RegistrarUsuarioRunner {

}
