package co.com.accenture.testAccenture.definitions;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.Matchers.equalTo;

import co.com.accenture.testAccenture.questions.ConsultaUsuarioId;
import co.com.accenture.testAccenture.tasks.RequestEliminarUsuario;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.rest.abiities.CallAnApi;
import net.thucydides.core.util.SystemEnvironmentVariables;

public class EliminarUsuario {
	
	private Actor accenture = Actor.named("Andres").whoCan(CallAnApi.at(SystemEnvironmentVariables.createEnvironmentVariables().getProperty("url")));
	
	@Given("^Yo como cliente deseo eliminar el usuario (\\d+)$")
	public void yo_como_cliente_deseo_eliminar_un_usuario(int id) {
		 accenture.remember("id", id);
	}

	@When("^Yo consumo el servicio DELETE$")
	public void yo_consumo_el_servicio_DELETE_con_el() {
		accenture.attemptsTo(RequestEliminarUsuario.remove(accenture.recall("id")));
	}

	@Then("^Yo consulto el servicio GET para verificar la eliminacion con la (\\d+)$")
	public void yo_consulto_el_servicio_GET_para_verificar_la_eliminacion_con_la(Integer respuesta) {
		accenture.should(seeThat(ConsultaUsuarioId.correcto(), equalTo(respuesta)));
	}
}