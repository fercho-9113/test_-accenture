package co.com.accenture.testAccenture.definitions;

import java.util.List;

import co.com.accenture.testAccenture.models.Colegio;
import co.com.accenture.testAccenture.tasks.RequestObtenerLosPrimerosColegios;
import co.com.accenture.testAccenture.tasks.RequestUsuariosDeColegios;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.rest.abiities.CallAnApi;
import net.thucydides.core.util.SystemEnvironmentVariables;

public class ObtenerColegiosConUsuarios {
	
	private Actor accenture = Actor.named("Andres").whoCan(CallAnApi.at(SystemEnvironmentVariables.createEnvironmentVariables().getProperty("url")));;
	
	@Given("^Yo consumo el servicio para listar los colegios y tomar los (\\d+) primeros$")
	public void yo_consumo_el_servicio_para_listar_los_colegios_y_tomar_los_primeros(int numero) {
		accenture.attemptsTo(RequestObtenerLosPrimerosColegios.find(numero));
	}

	@When("^Yo consumo el servicio de obtener usuarios por colegio$")
	public void yo_consumo_el_servicio_de_obtener_usuarios_por_colegio() {
		List<Colegio> colegios = SerenityRest.lastResponse().jsonPath().getList("", Colegio.class);
		accenture.attemptsTo(RequestUsuariosDeColegios.find(colegios));
	}

}
