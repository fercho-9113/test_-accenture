package co.com.accenture.testAccenture.definitions;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.Matchers.equalTo;

import java.util.List;

import co.com.accenture.testAccenture.models.Usuario;
import co.com.accenture.testAccenture.questions.ConsultaUsuario;
import co.com.accenture.testAccenture.tasks.RequestCrearUsuario;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.rest.abiities.CallAnApi;
import net.serenitybdd.screenplay.rest.questions.ResponseConsequence;
import net.thucydides.core.util.SystemEnvironmentVariables;

public class RegistrarUsuario {
	
	private Actor accenture = Actor.named("Andres").whoCan(CallAnApi.at(SystemEnvironmentVariables.createEnvironmentVariables().getProperty("url")));;
	
	@Given("^Yo deseo registrar un usuario$")
	public void yo_deseo_registrar_un_usuario(List<Usuario> usuarios) {
		accenture.remember("usuario", usuarios.get(0));
	}

	@When("^Yo consumo el servicio POST de creacion de usuario$")
	public void yo_consumo_el_servicio_POST_de_creacion_de_usuario() {
		accenture.attemptsTo(RequestCrearUsuario.add(accenture.recall("usuario")));
	}

	@Then("^Yo verifico la respuesta del servicio (\\d+)$")
	public void yo_verifico_la_respuesta_del_servicio(int respuesta) {
		accenture.should(ResponseConsequence.seeThatResponse(response -> response.statusCode(respuesta)));
	}
	
	@Then("^Yo verifico que haya sido creado el usuario (\\d+)$")
	public void yo_verifico_que_haya_sido_creado_el_usuario(Integer respuesta) {
		accenture.should(seeThat(ConsultaUsuario.correcto(), equalTo(respuesta)));
	}
	
}
