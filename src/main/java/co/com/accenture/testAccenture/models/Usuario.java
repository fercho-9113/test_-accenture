package co.com.accenture.testAccenture.models;

public class Usuario {
	
	private int id;
	private String nombre;
	private String segundo_nombre;
	private String apellido;
	private String segundo_apellido;
	private int id_colegio;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getSegundo_nombre() {
		return segundo_nombre;
	}
	public void setSegundo_nombre(String segundo_nombre) {
		this.segundo_nombre = segundo_nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getSegundo_apellido() {
		return segundo_apellido;
	}
	public void setSegundo_apellido(String segundo_apellido) {
		this.segundo_apellido = segundo_apellido;
	}
	public int getId_colegio() {
		return id_colegio;
	}
	public void setId_colegio(int id_colegio) {
		this.id_colegio = id_colegio;
	}

	
}
