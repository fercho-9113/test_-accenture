package co.com.accenture.testAccenture.questions;

import co.com.accenture.testAccenture.tasks.RequestConsultarUsuario;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class ConsultaUsuarioId implements Question<Integer>{

	@Override
	public Integer answeredBy(Actor actor) {
		actor.attemptsTo(RequestConsultarUsuario.find(actor.recall("id")));
		Integer respuesta = SerenityRest.lastResponse().getStatusCode();
		return respuesta;
	}
	
	public static ConsultaUsuarioId correcto() {
		return new ConsultaUsuarioId();
	}

}
