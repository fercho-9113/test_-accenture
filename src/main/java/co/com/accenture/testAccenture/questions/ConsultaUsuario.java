package co.com.accenture.testAccenture.questions;

import co.com.accenture.testAccenture.models.Usuario;
import co.com.accenture.testAccenture.tasks.RequestConsultarUsuario;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class ConsultaUsuario implements Question<Integer>{

	@Override
	public Integer answeredBy(Actor actor) {
		Usuario usuario= actor.recall("usuario");
		actor.attemptsTo(RequestConsultarUsuario.find(usuario.getId()));
		Integer respuesta = SerenityRest.lastResponse().getStatusCode();
		return respuesta;
	}
	
	public static ConsultaUsuario correcto() {
		return new ConsultaUsuario();
	}

}
