package co.com.accenture.testAccenture.tasks;

import static net.serenitybdd.screenplay.Tasks.instrumented;

import com.google.gson.JsonObject;

import co.com.accenture.testAccenture.models.Usuario;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.rest.interactions.Post;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;

public class RequestCrearUsuario implements Task{
	
	private Usuario usuario;
	private EnvironmentVariables variables = SystemEnvironmentVariables.createEnvironmentVariables();
	
	public RequestCrearUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@Step("Se completa la peticion y se realiza la peticion de creacion de usuario")
    @Override
    public <T extends Actor> void performAs(T actor) {
        JsonObject datosSolicitud = new JsonObject();
        datosSolicitud.addProperty(variables.getProperty("user.id"), usuario.getId());
        datosSolicitud.addProperty(variables.getProperty("user.nombre"), usuario.getNombre());
        datosSolicitud.addProperty(variables.getProperty("user.segundo.nombre"), usuario.getSegundo_nombre());
        datosSolicitud.addProperty(variables.getProperty("user.apellido"), usuario.getApellido());
        datosSolicitud.addProperty(variables.getProperty("user.segundo.apellido"), usuario.getSegundo_apellido());
        datosSolicitud.addProperty(variables.getProperty("user.id.colegio"), usuario.getId_colegio());
        actor.attemptsTo(Post.to(variables.getProperty("url.usuario"))
                .with(request -> request
                        .header(variables.getProperty("var.content"), variables.getProperty("var.type")).body(datosSolicitud)));
    }
	
	public static RequestCrearUsuario add(Usuario usuario) {
		return instrumented(RequestCrearUsuario.class, usuario);
	}

}
