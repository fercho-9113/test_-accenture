package co.com.accenture.testAccenture.tasks;

import static net.serenitybdd.screenplay.Tasks.instrumented;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.rest.interactions.Delete;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.util.SystemEnvironmentVariables;

public class RequestEliminarUsuario implements Task{

	private int id;
	
	public RequestEliminarUsuario(int id) {
		this.id = id;
	}
	
	@Step("Se realiza la peticion de eliminacion con el #id del usuario")
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Delete.from(SystemEnvironmentVariables.createEnvironmentVariables().getProperty("url.usuario") + id));
    }

	public static RequestEliminarUsuario remove(int id) {
		return instrumented(RequestEliminarUsuario.class, id);
	}
}
