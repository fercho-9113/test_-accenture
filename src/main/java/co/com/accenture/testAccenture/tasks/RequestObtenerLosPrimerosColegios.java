package co.com.accenture.testAccenture.tasks;

import static net.serenitybdd.screenplay.Tasks.instrumented;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.rest.interactions.Get;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.util.SystemEnvironmentVariables;

public class RequestObtenerLosPrimerosColegios implements Task{
	
	private int numero;
	
	public RequestObtenerLosPrimerosColegios(int numero) {
		this.numero = numero;
	}

	@Step("Se realiza la consulta de los primeros #numero colegios")
	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(
				Get.resource(SystemEnvironmentVariables.createEnvironmentVariables().getProperty("url.colegios.ordenados.asc") + numero));
	}
	
	public static RequestObtenerLosPrimerosColegios find(int numero) {
		return instrumented(RequestObtenerLosPrimerosColegios.class, numero);
	}

}
