package co.com.accenture.testAccenture.tasks;

import static net.serenitybdd.screenplay.Tasks.instrumented;

import java.util.List;

import co.com.accenture.testAccenture.models.Colegio;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.rest.interactions.Get;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.util.SystemEnvironmentVariables;

public class RequestUsuariosDeColegios implements Task{
	
	private List<Colegio> colegios;
	private String aux="";
	
	public RequestUsuariosDeColegios(List<Colegio> colegios) {
		this.colegios = colegios;
	}

	@Step("Se consultan los usuarios que posee el o los #ids de colegios")
	@Override
	public <T extends Actor> void performAs(T actor) {
		for (int i = 0; i < colegios.size(); i++) {
			if (i==0) {
				aux= SystemEnvironmentVariables.createEnvironmentVariables().getProperty("url.aux") + colegios.get(i).getId();
			}else {
				aux = aux + "&id_colegio="+ colegios.get(i).getId();
			} 
		}
		actor.attemptsTo(
				Get.resource(SystemEnvironmentVariables.createEnvironmentVariables().getProperty("url.usuario") + aux));
		
	}
	
	public static RequestUsuariosDeColegios find(List<Colegio> colegios) {
		return instrumented(RequestUsuariosDeColegios.class, colegios);
	}

}
