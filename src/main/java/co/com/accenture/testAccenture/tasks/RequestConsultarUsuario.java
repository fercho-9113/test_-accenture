package co.com.accenture.testAccenture.tasks;

import static net.serenitybdd.screenplay.Tasks.instrumented;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.rest.interactions.Get;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.util.SystemEnvironmentVariables;

public class RequestConsultarUsuario implements Task{
	
	private int id;
	
	public RequestConsultarUsuario (int id) {
		this.id=id;
	}

	@Step("Se consulta el usuario por medio del campo #id")
	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(
				Get.resource(SystemEnvironmentVariables.createEnvironmentVariables().getProperty("url.usuario")+id));		
	}
	
	public static RequestConsultarUsuario find(int id) {
		return instrumented(RequestConsultarUsuario.class, id);
	}


}
